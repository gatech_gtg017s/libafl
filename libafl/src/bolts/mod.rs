//! Bolts are no conceptual fuzzing elements, but they keep libafl-based fuzzers together.

pub mod anymap;
#[cfg(all(
    any(feature = "cli", feature = "frida_cli", feature = "qemu_cli"),
    feature = "std"
))]
pub mod cli;
#[cfg(feature = "llmp_compression")]
pub mod compress;
pub mod cpu;
#[cfg(feature = "std")]
pub mod fs;
#[cfg(feature = "std")]
pub mod launcher;
pub mod llmp;
#[cfg(all(feature = "std", unix))]
pub mod minibsod;
pub mod os;
pub mod ownedref;
pub mod rands;
pub mod serdeany;
pub mod shmem;
#[cfg(feature = "std")]
pub mod staterestore;
pub mod tuples;

use alloc::string::String;
use core::{iter::Iterator, time};
#[cfg(feature = "std")]
use std::time::{SystemTime, UNIX_EPOCH};

/// Can be converted to a slice
pub trait AsSlice<T> {
    /// Convert to a slice
    fn as_slice(&self) -> &[T];
}

/// Can be converted to a mutable slice
pub trait AsMutSlice<T> {
    /// Convert to a slice
    fn as_mut_slice(&mut self) -> &mut [T];
}

/// Create an `Iterator` from a reference
pub trait AsRefIterator<'it> {
    /// The item type
    type Item: 'it;
    /// The iterator type
    type IntoIter: Iterator<Item = &'it Self::Item>;

    /// Create an interator from &self
    fn as_ref_iter(&'it self) -> Self::IntoIter;
}

/// Create an `Iterator` from a mutable reference
pub trait AsMutIterator<'it> {
    /// The item type
    type Item: 'it;
    /// The iterator type
    type IntoIter: Iterator<Item = &'it mut Self::Item>;

    /// Create an interator from &mut self
    fn as_mut_iter(&'it mut self) -> Self::IntoIter;
}

/// Has a length field
pub trait HasLen {
    /// The length
    fn len(&self) -> usize;

    /// Returns `true` if it has no elements.
    fn is_empty(&self) -> bool {
        self.len() == 0
    }
}

/// Has a ref count
pub trait HasRefCnt {
    /// The ref count
    fn refcnt(&self) -> isize;
    /// The ref count, mutable
    fn refcnt_mut(&mut self) -> &mut isize;
}

/// Current time
#[cfg(feature = "std")]
#[must_use]
#[inline]
pub fn current_time() -> time::Duration {
    SystemTime::now().duration_since(UNIX_EPOCH).unwrap()
}

// external defined function in case of `no_std`
//
// Define your own `external_current_millis()` function via `extern "C"`
// which is linked into the binary and called from here.
#[cfg(not(feature = "std"))]
extern "C" {
    //#[no_mangle]
    fn external_current_millis() -> u64;
}

/// Current time (fixed fallback for `no_std`)
#[cfg(not(feature = "std"))]
#[inline]
#[must_use]
pub fn current_time() -> time::Duration {
    let millis = unsafe { external_current_millis() };
    time::Duration::from_millis(millis)
}

/// Gets current nanoseconds since [`UNIX_EPOCH`]
#[must_use]
#[inline]
pub fn current_nanos() -> u64 {
    current_time().as_nanos() as u64
}

/// Gets current milliseconds since [`UNIX_EPOCH`]
#[must_use]
#[inline]
pub fn current_milliseconds() -> u64 {
    current_time().as_millis() as u64
}

/// Format a `Duration` into a HMS string
#[must_use]
pub fn format_duration_hms(duration: &time::Duration) -> String {
    let secs = duration.as_secs();
    format!("{}h-{}m-{}s", (secs / 60) / 60, (secs / 60) % 60, secs % 60)
}


/// enum for caching value at memory address of different sizes
#[derive(Debug)]
pub enum MemoryAddress {
    /// 8-bit
    Mem8(u8),
    /// 16-bit
    Mem16(u16),
    /// 32-bit
    Mem32(u32),
    /// 64-bit
    Mem64(u64),
    /// 128-bit
    Mem128(u128)
}

impl From<*mut u8> for MemoryAddress {
    fn from(addr: *mut u8) -> MemoryAddress {
        MemoryAddress::Mem8(unsafe {*addr})
    }
}
impl From<*mut u16> for MemoryAddress {
    fn from(addr: *mut u16) -> MemoryAddress {
        MemoryAddress::Mem16(unsafe {*addr})
    }
}
impl From<*mut u32> for MemoryAddress {
    fn from(addr: *mut u32) -> MemoryAddress {
        MemoryAddress::Mem32(unsafe {*addr})
    }
}
impl From<*mut u64> for MemoryAddress {
    fn from(addr: *mut u64) -> MemoryAddress {
        MemoryAddress::Mem64(unsafe {*addr})
    }
}
impl From<*mut u128> for MemoryAddress {
    fn from(addr: *mut u128) -> MemoryAddress {
        MemoryAddress::Mem128(unsafe {*addr})
    }
}

impl MemoryAddress {
    /// return true of address still contains same value
    pub fn addr_match(&self, addr: u64) -> bool {
        match self {
            MemoryAddress::Mem8(v) => {
                let ptr = addr as *const u8;
                let val = unsafe {*ptr};
                *v == val
            }
            MemoryAddress::Mem16(v) => {
                let ptr = addr as *const u16;
                let val = unsafe {*ptr};
                *v == val
            }
            MemoryAddress::Mem32(v) => {
                let ptr = addr as *const u32;
                let val = unsafe {*ptr};
                *v == val
            }
            MemoryAddress::Mem64(v) => {
                let ptr = addr as *const u64;
                let val = unsafe {*ptr};
                *v == val
            }
            MemoryAddress::Mem128(v) => {
                let ptr = addr as *const u128;
                let val = unsafe {*ptr};
                *v == val
            }
        }
    }

    /// get cur value in address
    pub fn addr_new_val(&self, addr: u64) -> u64 {
        match self {
            MemoryAddress::Mem8(_v) => {
                let ptr = addr as *const u8;
                let val = unsafe {*ptr};
                val.into()
            }
            MemoryAddress::Mem16(_v) => {
                let ptr = addr as *const u16;
                let val = unsafe {*ptr};
                val.into()
            }
            MemoryAddress::Mem32(_v) => {
                let ptr = addr as *const u32;
                let val = unsafe {*ptr};
                val.into()
            }
            MemoryAddress::Mem64(_v) => {
                let ptr = addr as *const u64;
                let val = unsafe {*ptr};
                val.into()
            }
            MemoryAddress::Mem128(_v) => {
                let ptr = addr as *const u128;
                let val = unsafe {*ptr};
                val as u64
            }
        }
    }

    /// get saved value in address
    pub fn addr_old_val(&self) -> u64 {
        match self {
            MemoryAddress::Mem8(v) => {
                (*v).into()
            }
            MemoryAddress::Mem16(v) => {
                (*v).into()
            }
            MemoryAddress::Mem32(v) => {
                (*v).into()
            }
            MemoryAddress::Mem64(v) => {
                (*v).into()
            }
            MemoryAddress::Mem128(v) => {
                *v as u64
            }
        }
    }
}
