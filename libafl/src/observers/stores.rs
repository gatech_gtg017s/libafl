//! Observe inputs on behalf of trace-stores feedback (calibration)
//
// Copyright (C) 2022  Trevor Bentley
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

use std::sync::atomic::{AtomicBool, Ordering};
use crate::observers::Observer;
use crate::Error;
use crate::executors::ExitKind;
use crate::bolts::tuples::Named;
use crate::bolts::HasLen;
use crate::bolts::MemoryAddress;
use crate::bolts::rands::{Rand, StdRand};
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use serde::ser::SerializeStruct;

use std::collections::BTreeMap;
use std::collections::VecDeque;
use std::sync::{Arc, Mutex};

static HISTORY_COUNT: usize = 5000;
static HISTORY_MIN: usize = 1000;

/// x
#[derive(Debug, Clone)]
pub struct StoresObserver {
    name: String,
    seen_inputs: usize,
    injected_inputs: usize,
    skip_inputs: usize,
    calibration_inputs: usize,
    calibrating: bool,
    target_ratio: f64,
    interestingness: usize,
    history: VecDeque<bool>,
    atomic: Option<&'static AtomicBool>,
    cache: Option<Arc<Mutex<BTreeMap<u64, MemoryAddress>>>>,
    rng: StdRand,
}

impl Serialize for StoresObserver {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer {
        let mut s = serializer.serialize_struct("StoresObserver", 4)?;
        s.serialize_field("name", &self.name)?;
        s.serialize_field("seen_inputs", &self.seen_inputs)?;
        s.serialize_field("skip_inputs", &self.skip_inputs)?;
        s.serialize_field("calibration_inputs", &self.calibration_inputs)?;
        s.serialize_field("calibrating", &self.calibrating)?;
        s.end()
    }
}

impl<'de> Deserialize<'de> for StoresObserver {
    fn deserialize<D>(_deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de> {
        Ok(Self {
            name: "stores".into(),
            seen_inputs: 0,
            injected_inputs: 0,
            skip_inputs: 0,
            calibration_inputs: 0,
            calibrating: false,
            target_ratio: 0.0,
            interestingness: 0,
            history: VecDeque::with_capacity(HISTORY_COUNT),
            atomic: None,
            cache: None,
            rng: StdRand::default(),
        })
    }
}

impl StoresObserver {
    /// Creates a new [`StoresObserver`] with the given name.
    #[must_use]
    pub fn new(name: &'static str,
               skip_inputs: usize,
               calibration_inputs: usize,
               target_ratio: f64,
               atomic: &'static AtomicBool,
               cache: Arc<Mutex<BTreeMap<u64, MemoryAddress>>>) -> Self {
        let ratio = match target_ratio {
            v if v > 100.0 => 100.0,
            v if v < 0.0 => 0.0,
            _ => target_ratio,
        };
        Self {
            name: name.to_string(),
            seen_inputs: 0,
            injected_inputs: 0,
            skip_inputs,
            calibration_inputs,
            calibrating: false,
            target_ratio: ratio,
            interestingness: 0,
            history: VecDeque::with_capacity(HISTORY_COUNT),
            atomic: Some(atomic),
            cache: Some(cache),
            rng: StdRand::default(),
        }
    }
}

impl<I, S> Observer<I, S> for StoresObserver {
    fn pre_exec(&mut self, _state: &mut S, _input: &I) -> Result<(), Error> {
        if let Some(ref arcmut) = self.cache {
            if let Ok(mut cache) = arcmut.lock() {
                cache.clear();
            }
        }

        if self.seen_inputs >= self.calibration_inputs {
            if self.calibrating {
                println!("DISABLE CALIBRATE (seen: {})", self.seen_inputs);
                if let Some(atomic) = self.atomic {
                    atomic.store(false, Ordering::Relaxed);
                    self.calibrating = false;
                }
            }
        }
        else {
            if self.seen_inputs >= self.skip_inputs &&
                !self.calibrating {
                    println!("ENABLE CALIBRATE (seen: {})", self.seen_inputs);
                    if let Some(atomic) = self.atomic {
                        atomic.store(true, Ordering::Relaxed);
                        self.calibrating = true;
                    }
                }
        }
        Ok(())
    }

    fn post_exec(
        &mut self,
        _state: &mut S,
        _input: &I,
        _exit_kind: &ExitKind,
    ) -> Result<(), Error> {
        self.seen_inputs += 1;
        self.interestingness = 0;
        //println!("CALIBRATE SEEN INPUTS: {}", self.seen_inputs);
        if let Some(ref arcmut) = self.cache {
            if let Ok(cache) = arcmut.lock() {
                for (k, v) in cache.iter() {
                    // TODO: deal with less-than-64-bit
                    //let addr = *k as *const u64;
                    //let newval = unsafe { *addr };
                    //if *v != newval {
                    if !v.addr_match(*k) {
                        //println!(" - trace-stores count mod @ 0x{:08x}: 0x{:x} -> 0x{:x}",
                        //         k, v.addr_old_val(), v.addr_new_val(*k));
                        self.interestingness += 1;
                    }
                }
                if self.interestingness > 0 {
                    //println!("Last input trace-stores count: {}/{}", self.interestingness, cache.len());
                }
            }
        }
        while self.history.len() >= HISTORY_COUNT {
            let _ = self.history.pop_front();
        }
        let len = self.history.len();
        self.history.push_back(self.interestingness > 0);
        let count: usize = self.history.iter().filter(|x| **x).count();
        let ratio: f64 = ((count as f64) / (len as f64)) * 100.0;

        let ratio_diff = match self.target_ratio > ratio {
            true => self.target_ratio - ratio,
            false => 0.0,
        };
        // increase probability of injection as actual ratio drifts
        // farther away from target, and decrease as it drifts closer.
        // Keep a minimum of 10% so it keeps a good chance of actually
        // reaching the target.
        let choose_prob = match (ratio_diff / self.target_ratio) * 100.0 {
            v if v < 10.0 => 10.0,
            v => v,
        };
        let r = self.rng.between(0, 99) as f64;

        if self.interestingness == 0 &&
            len >= HISTORY_MIN &&
            ratio < self.target_ratio &&
            r < choose_prob {
                //println!("Padded with extra HW test-case!");
                //println!("HW ratio: {:.1} -> {:.1} ({} / {}) [{} / {}] [prob: {} ({})]",
                //         ratio, self.target_ratio, count, len,
                //         self.injected_inputs, self.seen_inputs,
                //         choose_prob, r);
                self.interestingness = 1;
                let _ = self.history.pop_front();
                self.history.push_back(true);
                self.injected_inputs += 1;
            }
        Ok(())
    }
}

impl Named for StoresObserver {
    fn name(&self) -> &str {
        &self.name
    }
}

impl HasLen for StoresObserver {
    fn len(&self) -> usize {
        self.interestingness
    }
}
