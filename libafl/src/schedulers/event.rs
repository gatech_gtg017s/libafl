//! The queue corpus scheduler implements an AFL-like queue mechanism

use alloc::borrow::ToOwned;
use crate::executors::{Executor, HasObservers};
use crate::events::EventManager;
use crate::observers::ObserversTuple;
use crate::state::{HasClientPerfMonitor, HasExecutions};
use crate::{corpus::Corpus, inputs::Input, schedulers::Scheduler, state::HasCorpus, Error};
use core::marker::PhantomData;

use crate::executors::inprocess::GLOBAL_STATE;

/// Walk the corpus in a queue-like fashion
#[derive(Debug)]
pub struct EventScheduler<I, S, E, EM, OT, FZ, ST> {
    phantom: PhantomData<(E, EM, I, OT, S, FZ, ST)>,
}

impl<I, S, E, EM, OT, FZ, ST> Scheduler<I, S> for EventScheduler<I, S, E, EM, OT, FZ, ST>
where
    S: HasCorpus<I> + HasClientPerfMonitor + HasExecutions,
    I: Input,
    E: Executor<EM, I, S, FZ> + HasObservers<I, OT, S>,
    EM: EventManager<E, I, S, FZ>,
    OT: ObserversTuple<I, S> + serde::Serialize + serde::de::DeserializeOwned,
    ST: crate::stages::StagesTuple<E, EM, S, FZ>,
    FZ: crate::fuzzer::Fuzzer<E, EM, I, S, ST>,
{
    /// Gets the next entry in the queue
    fn next(&self, state: &mut S) -> Result<usize, Error> {
        if state.corpus().count() == 0 {
            Err(Error::Empty("No entries in corpus".to_owned()))
        } else {
            //let id = match state.corpus().current() {
            //    Some(cur) => {
            //        *cur + 1
            //    }
            //    None => 0,
            //};
            let mut id = state.corpus().current().unwrap_or(std::usize::MAX);

            let _data = unsafe { &mut GLOBAL_STATE };
            let _executor = unsafe { (_data.executor_ptr as *mut E).as_mut().unwrap() };
            let _fuzzer = unsafe { (_data.fuzzer_ptr as *mut FZ).as_mut().unwrap() };
            let _mgr = unsafe { (_data.event_mgr_ptr as *mut EM).as_mut().unwrap() };
            while id >= state.corpus().count() {
                //self.mgr.process(self, state, &mut self.executor)?;
                _mgr.process(_fuzzer, state, _executor)?;
                id = state.corpus().current().unwrap_or(std::usize::MAX);
            }

            *state.corpus_mut().current_mut() = Some(id);
            Ok(id)
        }
    }
}

impl<I, S, E, EM, OT, FZ, ST> EventScheduler<I, S, E, EM, OT, FZ, ST>
where
    E: Executor<EM, I, S, FZ> + HasObservers<I, OT, S>,
    EM: EventManager<E, I, S, FZ>,
    S: HasCorpus<I> + HasClientPerfMonitor + HasExecutions,
    I: Input,
    OT: ObserversTuple<I, S> + serde::Serialize + serde::de::DeserializeOwned,
    ST: crate::stages::StagesTuple<E, EM, S, FZ>,
    FZ: crate::fuzzer::Fuzzer<E, EM, I, S, ST>,
{
    /// Creates a new `EventScheduler`
    #[must_use]
    pub fn new() -> Self {
        Self {
            phantom: PhantomData,
        }
    }
}
