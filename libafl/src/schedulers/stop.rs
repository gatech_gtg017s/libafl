//! The queue corpus scheduler implements an AFL-like queue mechanism
//
// Copyright (C) 2022  Trevor Bentley
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

use alloc::borrow::ToOwned;

use crate::{corpus::Corpus, inputs::Input, schedulers::Scheduler, state::HasCorpus, Error};

/// Walk the corpus in a queue-like fashion
#[derive(Debug, Clone)]
pub struct StopScheduler;

impl<I, S> Scheduler<I, S> for StopScheduler
where
    S: HasCorpus<I>,
    I: Input,
{
    /// Gets the next entry in the queue
    fn next(&self, state: &mut S) -> Result<usize, Error> {
        if state.corpus().count() == 0 {
            Err(Error::Empty("No entries in corpus".to_owned()))
        } else {
            let id = match state.corpus().current() {
                Some(cur) => {
                    if *cur + 1 >= state.corpus().count() {
                        std::usize::MAX
                    } else {
                        *cur + 1
                    }
                }
                None => 0,
            };
            if id == std::usize::MAX {
                Err(Error::Empty("Corpus finished replaying".to_owned()))
            }
            else {
                *state.corpus_mut().current_mut() = Some(id);
                Ok(id)
            }
        }
    }
}

impl StopScheduler {
    /// Creates a new `StopScheduler`
    #[must_use]
    pub fn new() -> Self {
        Self
    }
}

impl Default for StopScheduler {
    fn default() -> Self {
        Self::new()
    }
}
