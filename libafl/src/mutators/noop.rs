//! The `ScheduledMutator` schedules multiple mutations internally.
//
// Copyright (C) 2022  Trevor Bentley
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

use core::{
    fmt::{self, Debug},
    marker::PhantomData,
};

use crate::{
    inputs::Input,
    mutators::{MutationResult, Mutator},
    state::HasRand,
    Error,
};

pub use crate::mutators::mutations::*;
pub use crate::mutators::token_mutations::*;

/// A [`Mutator`] scheduling multiple [`Mutator`]s for an input.
pub trait NoopMutator<I, S>: Mutator<I, S>
where
    I: Input,
{
    /// New default implementation for mutate.
    /// Implementations must forward mutate() to this method
    fn scheduled_mutate(
        &mut self,
        _state: &mut S,
        _input: &mut I,
        _stage_idx: i32,
    ) -> Result<MutationResult, Error> {
        let r = MutationResult::Skipped;
        //println!("MUTATE: {:?}", _input);
        Ok(r)
    }
}

/// A [`Mutator`] that schedules one of the embedded mutations on each call.
pub struct StdNoopMutator<I, S>
where
    I: Input,
    S: HasRand,
{
    phantom: PhantomData<(I, S)>,
}

impl<I, S> Mutator<I, S> for StdNoopMutator<I, S>
where
    I: Input,
    S: HasRand,
{
    #[inline]
    fn mutate(
        &mut self,
        state: &mut S,
        input: &mut I,
        stage_idx: i32,
    ) -> Result<MutationResult, Error> {
        self.scheduled_mutate(state, input, stage_idx)
    }
}

impl<I, S> NoopMutator<I, S> for StdNoopMutator<I, S>
where
    I: Input,
    S: HasRand,
{
}

impl<I, S> Debug for StdNoopMutator<I, S>
where
    I: Input,
    S: HasRand,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "StdNoopMutator for Input type {}",
            core::any::type_name::<I>()
        )
    }
}

impl<I, S> StdNoopMutator<I, S>
where
    I: Input,
    S: HasRand,
{
    /// Create a new [`StdScheduledMutator`] instance specifying mutations
    pub fn new() -> Self {
        StdNoopMutator {
            phantom: PhantomData,
        }
    }
}
