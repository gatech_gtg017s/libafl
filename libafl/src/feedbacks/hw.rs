//! Make a feedback return matches as interesting to hardware devices
//
// Copyright (C) 2022  Trevor Bentley
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

use crate::{
    feedbacks::Feedback,
    bolts::tuples::Named,
    bolts::rands::{Rand, StdRand},
    corpus::Testcase,
    events::{Event, EventFirer},
    executors::ExitKind,
    inputs::Input,
    monitors::UserStats,
    observers::ObserversTuple,
    state::HasClientPerfMonitor,
    Error,
};

use core::{
    fmt::{self, Debug, Formatter},
    marker::PhantomData,
};


/// Wrap a normal feedback, making it indicate that interesting
/// testcases are relevant to hardware.
#[derive(Clone)]
pub struct HwFeedback<A, I, S>
where
    A: Feedback<I, S>,
    I: Input,
    S: HasClientPerfMonitor,
{
    /// The feedback to convert to hardware
    pub first: A,
    /// The name
    name: String,
    input_interesting: u64,
    input_total: u64,
    phantom: PhantomData<(I, S)>,
}

impl<A, I, S> Debug for HwFeedback<A, I, S>
where
    A: Feedback<I, S>,
    I: Input,
    S: HasClientPerfMonitor,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.debug_struct("HwFeedback")
            .field("name", &self.name)
            .field("first", &self.first)
            .finish()
    }
}

impl<A, I, S> Feedback<I, S> for HwFeedback<A, I, S>
where
    A: Feedback<I, S>,
    I: Input,
    S: HasClientPerfMonitor,
{
    #[allow(clippy::wrong_self_convention)]
    fn is_interesting<EM, OT>(
        &mut self,
        _state: &mut S,
        _manager: &mut EM,
        _input: &I,
        _observers: &OT,
        _exit_kind: &ExitKind,
    ) -> Result<bool, Error>
    where
        EM: EventFirer<I>,
        OT: ObserversTuple<I, S>,
    {
        // TREV TODO: is_interesting() has side effects, can only be called once per input

        //let intr = self.first.is_interesting(_state, _manager, _input, _observers, _exit_kind)?;
        //if intr {
        //    println!("HW feedback interesting (normal)!! {}", intr);
        //    //loop {}
        //}
        Ok(false)
    }

    #[allow(clippy::wrong_self_convention)]
    fn is_interesting_hardware<EM, OT>(
        &mut self,
        state: &mut S,
        manager: &mut EM,
        input: &I,
        observers: &OT,
        exit_kind: &ExitKind,
    ) -> Result<bool, Error>
    where
        EM: EventFirer<I>,
        OT: ObserversTuple<I, S>,
    {
        let intr = self.first.is_interesting(state, manager, input, observers, exit_kind)?;
        self.input_total += 1;
        if intr {
            //println!("HW feedback interesting (hw)!!! {}", intr);
            self.input_interesting += 1;
        }
        if intr && (self.input_interesting % 20000 == 0 || self.input_total % 100000 == 0) {
            //println!("UPDATE USER STATS: {} / {}", self.input_interesting, self.input_total);
            manager.fire(
                state,
                Event::UpdateUserStats {
                    name: self.name.to_string(),
                    value: UserStats::Ratio(self.input_interesting, self.input_total),
                    phantom: PhantomData,
                },
            )?;
        }
        Ok(intr)
    }

    #[inline]
    fn append_metadata(&mut self, state: &mut S, testcase: &mut Testcase<I>) -> Result<(), Error> {
        self.first.append_metadata(state, testcase)
    }

    #[inline]
    fn discard_metadata(&mut self, state: &mut S, input: &I) -> Result<(), Error> {
        self.first.discard_metadata(state, input)
    }
}

impl<A, I, S> Named for HwFeedback<A, I, S>
where
    A: Feedback<I, S>,
    I: Input,
    S: HasClientPerfMonitor,
{
    #[inline]
    fn name(&self) -> &str {
        &self.name
    }
}

impl<A, I, S> HwFeedback<A, I, S>
where
    A: Feedback<I, S>,
    I: Input,
    S: HasClientPerfMonitor,
{
    /// Creates a new [`HwFeedback`].
    pub fn new(first: A) -> Self {
        let name = format!("Hw({})", first.name());
        Self {
            first,
            name,
            input_interesting: 0,
            input_total: 0,
            phantom: PhantomData,
        }
    }
}





/// Considers all inputs interesting
#[derive(Clone, Debug)]
pub struct AllFeedback {
    name: String,
}

impl<I, S> Feedback<I, S> for AllFeedback
where
    I: Input,
    S: HasClientPerfMonitor,
{
    #[allow(clippy::wrong_self_convention)]
    fn is_interesting<EM, OT>(
        &mut self,
        _state: &mut S,
        _manager: &mut EM,
        _input: &I,
        _observers: &OT,
        _exit_kind: &ExitKind,
    ) -> Result<bool, Error>
    where
        EM: EventFirer<I>,
        OT: ObserversTuple<I, S>,
    {
        Ok(true)
    }
}

impl Named for AllFeedback {
    #[inline]
    fn name(&self) -> &str {
        self.name.as_str()
    }
}

impl AllFeedback {
    /// AllFeedback
    #[must_use]
    pub fn new(name: &'static str) -> Self {
        Self {
            name: name.to_string(),
        }
    }
}



/// Considers all inputs interesting
#[derive(Clone, Debug)]
pub struct RandFeedback {
    name: String,
    probability: u8,
    rng: StdRand,
}

impl<I, S> Feedback<I, S> for RandFeedback
where
    I: Input,
    S: HasClientPerfMonitor,
{
    #[allow(clippy::wrong_self_convention)]
    fn is_interesting<EM, OT>(
        &mut self,
        _state: &mut S,
        _manager: &mut EM,
        _input: &I,
        _observers: &OT,
        _exit_kind: &ExitKind,
    ) -> Result<bool, Error>
    where
        EM: EventFirer<I>,
        OT: ObserversTuple<I, S>,
    {
        let r = self.rng.between(0, 99);
        Ok(r < self.probability.into())
        //Ok(*self.rng.choose(&[true, true, true, false]))
    }
}

impl Named for RandFeedback {
    #[inline]
    fn name(&self) -> &str {
        self.name.as_str()
    }
}

impl RandFeedback {
    /// RandFeedback
    #[must_use]
    pub fn new(name: &'static str, probability: u8) -> Self {
        Self {
            name: name.to_string(),
            probability,
            rng: StdRand::default(),
        }
    }
}
