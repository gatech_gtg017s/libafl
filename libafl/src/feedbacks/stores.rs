//! Nop feedback for trace-stores calibration
//
// Copyright (C) 2022  Trevor Bentley
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

use crate::{
    bolts::tuples::Named,
    bolts::HasLen,
    corpus::Testcase,
    events::EventFirer,
    executors::ExitKind,
    feedbacks::Feedback,
    inputs::Input,
    observers::{ObserversTuple, StoresObserver},
    state::HasClientPerfMonitor,
    Error,
};

/// x
#[derive(Clone, Debug)]
pub struct StoresFeedback {
    name: String,
}

impl<I, S> Feedback<I, S> for StoresFeedback
where
    I: Input,
    S: HasClientPerfMonitor,
{
    #[allow(clippy::wrong_self_convention)]
    fn is_interesting<EM, OT>(
        &mut self,
        _state: &mut S,
        _manager: &mut EM,
        _input: &I,
        observers: &OT,
        _exit_kind: &ExitKind,
    ) -> Result<bool, Error>
    where
        EM: EventFirer<I>,
        OT: ObserversTuple<I, S>,
    {
        let observer = observers.match_name::<StoresObserver>(self.name()).unwrap();
        //println!("Is interesting trace-stores count: {}", observer.len());
        Ok(!observer.is_empty())
        //Ok(false)
    }

    /// Append to the testcase the generated metadata in case of a new corpus item
    #[inline]
    fn append_metadata(&mut self, _state: &mut S, _testcase: &mut Testcase<I>) -> Result<(), Error> {
        Ok(())
    }

    /// Discard the stored metadata in case that the testcase is not added to the corpus
    #[inline]
    fn discard_metadata(&mut self, _state: &mut S, _input: &I) -> Result<(), Error> {
        Ok(())
    }
}

impl Named for StoresFeedback {
    #[inline]
    fn name(&self) -> &str {
        self.name.as_str()
    }
}

impl StoresFeedback {
    /// Creates a new [`StoresFeedback`]
    #[must_use]
    pub fn new_with_observer(observer: &StoresObserver) -> Self {
        Self {
            name: observer.name().to_string(),
        }
    }
}
