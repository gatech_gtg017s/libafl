//! Calibration stage for trace-stores
//
// Copyright (C) 2022  Trevor Bentley
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

use core::marker::PhantomData;
use std::sync::atomic::{AtomicBool, Ordering};

use crate::{
    fuzzer::Evaluator,
    inputs::Input,
    stages::Stage,
    state::{HasClientPerfMonitor, HasCorpus, HasMetadata},
    events::EventFirer,
    executors::{Executor, HasObservers},
    observers::ObserversTuple,
    Error,
};

/// calibrate trace-stores stage
#[derive(Clone, Debug)]
pub struct CalibrateStoresStage<I, OT, S>
where
    I: Input,
    OT: ObserversTuple<I, S>,
    S: HasCorpus<I> + HasMetadata,
{
    seen_inputs: usize,
    calibration_inputs: usize,
    calibrating: bool,
    atomic: &'static AtomicBool,
    #[allow(clippy::type_complexity)]
    phantom: PhantomData<(I, OT, S)>,
}

impl<E, EM, S, Z, I, OT> Stage<E, EM, S, Z> for CalibrateStoresStage<I, OT, S>
where
    E: Executor<EM, I, S, Z> + HasObservers<I, OT, S>,
    EM: EventFirer<I>,
    I: Input,
    OT: ObserversTuple<I, S>,
    S: HasCorpus<I> + HasMetadata + HasClientPerfMonitor,
    Z: Evaluator<E, EM, I, S>,
{
    #[inline]
    #[allow(clippy::let_and_return)]
    fn perform(
        &mut self,
        _fuzzer: &mut Z,
        _executor: &mut E,
        _state: &mut S,
        _manager: &mut EM,
        _corpus_idx: usize,
    ) -> Result<(), Error> {
        if self.seen_inputs >= self.calibration_inputs {
            if self.calibrating {
                println!("DISABLE CALIBRATE");
                (*self.atomic).store(false, Ordering::Relaxed);
            }
        }
        else {
            if !self.calibrating {
                println!("ENABLE CALIBRATE");
                self.atomic.store(true, Ordering::Relaxed);
            }
        }
        self.seen_inputs += 1;
        println!("CALIBRATE SEEN INPUTS: {}", self.seen_inputs);
        Ok(())
    }
}

impl<I, OT, S> CalibrateStoresStage<I, OT, S>
where
    I: Input,
    OT: ObserversTuple<I, S>,
    S: HasCorpus<I> + HasMetadata,
{
    /// Creates a new default mutational stage
    pub fn new(calibration_inputs: usize, atomic: &'static AtomicBool) -> Self {
        Self {
            seen_inputs: 0,
            calibration_inputs,
            calibrating: false,
            atomic,
            phantom: PhantomData,
        }
    }
}
