//! x
//
// Copyright (C) 2022  Trevor Bentley
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

use std::collections::BTreeSet;
use std::sync::Mutex;
use std::sync::atomic::Ordering;
use lazy_static::lazy_static;
use crate::coverage::{
    //STORES_MAP,
    CALIBRATING_STORES, STORES_CACHE};
use crate::{libfuzzer_global_bss_range,
            libfuzzer_global_data_range,
            libfuzzer_global_nvm_range};
//use libafl::bolts::MemoryAddress;

//use libc::{
//    raise, SIGTRAP,
//};

// These callbacks can be called from any thread, and neither Mutex
// nor BTreeSet have const fn initializers, so use a lazy_static init
// to make a global reference that can be accessed via mutex.
lazy_static!{
    static ref IGNORED_ADDRS: Mutex<BTreeSet<u64>> = Mutex::new(BTreeSet::new());
}

unsafe fn sancov_store(addr: *mut u8) -> bool {
    let addr_u64 = addr as u64;
    let (bss_start, bss_end)   = libfuzzer_global_bss_range();
    let (data_start, data_end) = libfuzzer_global_data_range();
    let (nvm_start, nvm_end)   = libfuzzer_global_nvm_range();

    if (addr_u64 < bss_start || addr_u64 > bss_end) &&
        (addr_u64 < data_start || addr_u64 > data_end) &&
        (addr_u64 < nvm_start || addr_u64 > nvm_end) {
            return false;
        }

    if let Ok(mut guard) = IGNORED_ADDRS.lock() {
        if CALIBRATING_STORES.load(Ordering::Relaxed) {
            println!("sancov-stores: CALIBRATE {:08x}", addr_u64);
            guard.insert(addr_u64);
            return false;
        }
        else {
            if guard.contains(&addr_u64) {
                //println!("sancov-stores: IGNORE {:08x}", addr_u64);
                return false;
            }
            else {
                //println!("sancov-stores: ACCEPT {:08x}", addr_u64);
                //if addr_u64 == 0x142e400 {
                //    println!("PAUSE");
                //    raise(SIGTRAP);
                //}
            }
        }
    }
    //let pos: usize = ((addr as u64) & 0xFFFF) as usize;

    // for now, just always set the first entry
    //let pos: usize = 0;
    //let val = (*STORES_MAP.get_unchecked(pos)).saturating_add(1);
    //*STORES_MAP.get_unchecked_mut(pos) = val;
    //println!("sancov-stores: {:08x} -> {}", addr as u64, *STORES_MAP.get_unchecked(pos));
    true
}

/// 8-bit
#[no_mangle]
pub unsafe extern "C" fn  __sanitizer_cov_store1(addr: *mut u8) {
    if sancov_store(addr) {
        if let Ok(mut guard) = STORES_CACHE.lock() {
            let addr_u64 = addr as u64;
            if !guard.contains_key(&addr_u64) {
                guard.insert(addr_u64, addr.into());
            }
        }
    }
}

/// 16-bit
#[no_mangle]
pub unsafe extern "C" fn  __sanitizer_cov_store2(addr: *mut u16) {
    if sancov_store(addr as *mut u8) {
        if let Ok(mut guard) = STORES_CACHE.lock() {
            let addr_u64 = addr as u64;
            if !guard.contains_key(&addr_u64) {
                guard.insert(addr_u64, addr.into());
            }
        }
    }
}

/// 32-bit
#[no_mangle]
pub unsafe extern "C" fn  __sanitizer_cov_store4(addr: *mut u32) {
    if sancov_store(addr as *mut u8) {
        if let Ok(mut guard) = STORES_CACHE.lock() {
            let addr_u64 = addr as u64;
            if !guard.contains_key(&addr_u64) {
                guard.insert(addr_u64, addr.into());
            }
        }
    }
}

/// 64-bit
#[no_mangle]
pub unsafe extern "C" fn  __sanitizer_cov_store8(addr: *mut u64) {
    if sancov_store(addr as *mut u8) {
        if let Ok(mut guard) = STORES_CACHE.lock() {
            let addr_u64 = addr as u64;
            if !guard.contains_key(&addr_u64) {
                guard.insert(addr_u64, addr.into());
            }
        }
    }
}

/// 128-bit
#[no_mangle]
pub unsafe extern "C" fn  __sanitizer_cov_store16(addr: *mut u128) {
    if sancov_store(addr as *mut u8) {
        if let Ok(mut guard) = STORES_CACHE.lock() {
            let addr_u64 = addr as u64;
            if !guard.contains_key(&addr_u64) {
                guard.insert(addr_u64, addr.into());
            }
        }
    }
}
