//! [`Libfuzzer`](https://www.llvm.org/docs/LibFuzzer.html)-style runtime wrapper for `LibAFL`.
//! This makes `LibAFL` interoperable with harnesses written for other fuzzers like `Libfuzzer` and [`AFLplusplus`](aflplus.plus).
//! We will interact with a C++ target, so use external c functionality

extern "C" {
    /// int LLVMFuzzerTestOneInput(const uint8_t *Data, size_t Size)
    fn LLVMFuzzerTestOneInput(data: *const u8, size: usize) -> i32;
    fn LLVMFuzzerTestOneInputHw(data: *const u8, size: usize) -> i32;

    //fn LLVMFuzzerGlobalAddressRange(start: *mut u64, end: *mut u64) -> i32;

    // libafl_targets_libfuzzer_init calls LLVMFUzzerInitialize()
    fn libafl_targets_libfuzzer_init(argc: *const i32, argv: *const *const *const u8) -> i32;

    fn LLVMFuzzerInitializeHw(argc: *const i32, argv: *const *const *const u8) -> i32;

    static __start_target_bss: u64;
    static __stop_target_bss: u64;
    static __start_target_data: u64;
    static __stop_target_data: u64;
    static __start_target_nvm: u64;
    static __stop_target_nvm: u64;
}

/// Calls the (native) libfuzzer initialize function.
/// Returns the value returned by the init function.
/// # Safety
/// Calls the libfuzzer-style init function which is native code.
#[allow(clippy::similar_names)]
#[allow(clippy::must_use_candidate)] // nobody uses that return code...
pub fn libfuzzer_initialize(args: &[String]) -> i32 {
    let args: Vec<String> = args.iter().map(|x| x.clone() + "\0").collect();
    let argv: Vec<*const u8> = args.iter().map(|x| x.as_bytes().as_ptr()).collect();
    assert!(argv.len() < i32::MAX as usize);
    #[allow(clippy::cast_possible_wrap)]
    let argc = argv.len() as i32;
    unsafe {
        let argv_ptr = argv.as_ptr();
        libafl_targets_libfuzzer_init(core::ptr::addr_of!(argc), core::ptr::addr_of!(argv_ptr))
    }
}

/// Calls the libfuzzer-style init for secondary (hw) target
#[allow(clippy::similar_names)]
#[allow(clippy::must_use_candidate)] // nobody uses that return code...
pub fn libfuzzer_initialize_hw(args: &[String]) -> i32 {
    let args: Vec<String> = args.iter().map(|x| x.clone() + "\0").collect();
    let argv: Vec<*const u8> = args.iter().map(|x| x.as_bytes().as_ptr()).collect();
    assert!(argv.len() < i32::MAX as usize);
    #[allow(clippy::cast_possible_wrap)]
    let argc = argv.len() as i32;
    unsafe {
        let argv_ptr = argv.as_ptr();
        LLVMFuzzerInitializeHw(core::ptr::addr_of!(argc), core::ptr::addr_of!(argv_ptr))
    }
}

/// Call a single input of a libfuzzer-style cpp-harness
/// # Safety
/// Calls the libfuzzer harness. We actually think the target is unsafe and crashes eventually, that's why we do all this fuzzing.
#[allow(clippy::must_use_candidate)]
pub fn libfuzzer_test_one_input(buf: &[u8]) -> i32 {
    unsafe { LLVMFuzzerTestOneInput(buf.as_ptr(), buf.len()) }
}

/// Call libfuzzer harness of secondary (hw) target
#[allow(clippy::must_use_candidate)]
pub fn libfuzzer_test_one_input_hw(buf: &[u8]) -> i32 {
    unsafe { LLVMFuzzerTestOneInputHw(buf.as_ptr(), buf.len()) }
}

/// Get address range of global variables from target
pub fn libfuzzer_global_bss_range() -> (u64, u64) {
    let start: u64;
    let end: u64;
    unsafe {
        //assert!(LLVMFuzzerGlobalAddressRange(&mut start as *mut u64,
        //                                     &mut end as *mut u64) == 0);
        start = &__start_target_bss as *const u64 as u64;
        end = &__stop_target_bss as *const u64 as u64;
    }
    (start, end)
}

/// Get address range of global variables from target
pub fn libfuzzer_global_data_range() -> (u64, u64) {
    let start: u64;
    let end: u64;
    unsafe {
        //assert!(LLVMFuzzerGlobalAddressRange(&mut start as *mut u64,
        //                                     &mut end as *mut u64) == 0);
        start = &__start_target_data as *const u64 as u64;
        end = &__stop_target_data as *const u64 as u64;
    }
    (start, end)
}

/// Get address range of global variables from target
pub fn libfuzzer_global_nvm_range() -> (u64, u64) {
    let start: u64;
    let end: u64;
    unsafe {
        //assert!(LLVMFuzzerGlobalAddressRange(&mut start as *mut u64,
        //                                     &mut end as *mut u64) == 0);
        start = &__start_target_nvm as *const u64 as u64;
        end = &__stop_target_nvm as *const u64 as u64;
    }
    (start, end)
}
