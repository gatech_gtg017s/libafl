#!/bin/bash
set -e

if [[ $# -ne 2 ]]; then
    echo "usage: $0 <ratio> <idx>"
    exit 1
fi

RATIO=$1
IDX=$2

EXEC_ARR=(`cat multi_r${RATIO}_${IDX}_broker.log |grep "Hw(stores)" |tail -n1 |sed 's/.*executions: \([0-9]*\), exec[/]sec: \([0-9]*\).*Hw[(]stores[)]: \([0-9]*\).*/\1 \2 \3/'`)
EXEC_TOTAL=${EXEC_ARR[0]}
EXEC_RATE=${EXEC_ARR[1]}
EXEC_HW=${EXEC_ARR[2]}

SECONDS=`cat multi_r${RATIO}_${IDX}_broker.log |grep GLOBAL | tail -n1 |sed 's/.*run time: \([0-9]*\)h[-]\([0-9]*\)m[-]\([0-9]*\)s.*/\1 \2 \3/' | awk '{print $1*60*60 + $2*60 + $3}'`

ABORT_COUNT=`grep -A10 ABORTs multi_r${RATIO}_${IDX}_secondary.log | wc -l`
ABORTS="|||||||||||||||||||"
if [[ $ABORT_COUNT -eq 11 ]]; then
    ABORTS=`grep -A10 ABORTs multi_r${RATIO}_${IDX}_secondary.log |grep -v ABORT | sed 's/  - \([0-9]*\): \([0-9]*\) [(]\([0-9]*\).*/\3 \2/' | paste -sd" " |tr " " "|"`
fi

echo $ABORTS | awk -v ratio=$RATIO -v id=$IDX -v exec_total=$EXEC_TOTAL -v exec_hw=$EXEC_HW -v exec_rate=$EXEC_RATE -v seconds=$SECONDS 'BEGIN{ORS=""}{printf "|r%s_%s|%s|0|",ratio,id,ratio ; print ; printf "|%s|%s|%s|%s|%d||||\n",seconds,exec_total,exec_hw,exec_rate,exec_hw/seconds }' |awk -F'|' 'BEGIN{OFS="|"} {for (i=1;i<NF;i++) { if ($i ~ /^[0-9]+$/ && $i > 18446744072000000000) {$i=""} } ; print}'
