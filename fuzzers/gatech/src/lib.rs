//! A libfuzzer-like fuzzer with llmp-multithreading support and restarts
//
// zebrafuzz -- demonstration of a multi-target fuzzer in LibAFL
//
// NOTICE: LibAFL is distributed under the MIT and Apache licenses.
// This demo, and the additions to LibAFL to support it, are
// distributed under the AGPL-3.0 license.
//
// Copyright (C) 2022  Trevor Bentley
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
use mimalloc::MiMalloc;
#[global_allocator]
static GLOBAL: MiMalloc = MiMalloc;

use core::time::Duration;
use std::{env, path::PathBuf, thread};

use libafl::{
    bolts::{
        current_nanos,
        rands::StdRand,
        tuples::{tuple_list},
        AsSlice,
    },
    ExecutionProcessor,
    corpus::{Corpus, InMemoryCorpus, CachedOnDiskCorpus, OnDiskCorpus},
    //events::{setup_restarting_mgr_std, EventConfig, EventRestarter, SimpleEventManager},
    //events::SimpleEventManager,
    events::{setup_restarting_mgr_std, EventConfig},
    executors::{inprocess::InProcessExecutor, ExitKind, TimeoutExecutor},
    feedback_or, feedback_or_fast,
    feedbacks::{CrashFeedback, MapFeedbackState,
                //AnyMapFeedback,
                //AllFeedback,
                RandFeedback,
                RandomMapFeedback,
                //AlwaysMapFeedback,
                MaxMapFeedback, TimeFeedback, TimeoutFeedback,
                HwFeedback,
                StoresFeedback,
    },
    fuzzer::{Fuzzer, StdFuzzer},
    inputs::{BytesInput, HasTargetBytes},
    monitors::MultiMonitor,
    //mutators::scheduled::{havoc_mutations, StdScheduledMutator},
    mutators::scheduled::{havoc_mutations},
    mutators::StdMOptMutator,
    observers::{HitcountsMapObserver, StdMapObserver, TimeObserver, StoresObserver},
    schedulers::{
        powersched::PowerSchedule, IndexesLenTimeMinimizerScheduler, StdWeightedScheduler,
        //QueueScheduler,
    },
    stages::{calibrate::CalibrationStage,
             power::StdPowerMutationalStage,
             StdMutationalStage,
             //calibrate_stores::CalibrateStoresStage,
    },
    //stages::mutational::StdMutationalStage,
    state::{HasCorpus, StdState,
            //HasClientPerfMonitor, HasMetadata
    },
    //feedbacks::Feedback,
    //inputs::Input,
    //schedulers::Scheduler,
    Evaluator,
    Error,
};

use libafl_targets::{libfuzzer_initialize, libfuzzer_initialize_hw,
                     libfuzzer_test_one_input, libfuzzer_test_one_input_hw,
                     EDGES_MAP, MAX_EDGES_NUM,
                     //STORES_MAP, MAX_STORES_NUM,
                     STORES_CACHE,
                     CALIBRATING_STORES};

/// The main fn, `no_mangle` as it is a C main
#[cfg(not(test))]
#[no_mangle]
pub fn libafl_main() {
    // Registry the metadata types used in this fuzzer
    // Needed only on no_std
    //RegistryBuilder::register::<Tokens>();

    println!(
        "Workdir: {:?}",
        env::current_dir().unwrap().to_string_lossy().to_string()
    );

    let fuzz_type = env::var("HWFUZZ")
        .as_ref()
        .map(String::as_str)
        .map_or(Ok(0), str::parse)
        .expect("Failed to read HWFUZZ env var");
    let fuzz_fn = match fuzz_type {
        1 => hw_fuzz,
        2 => fuzz_single_blackbox,
        _ => fuzz,
    };
    let crash_dir = match fuzz_type {
        1 => PathBuf::from("./crashes_hw"),
        _ => PathBuf::from("./crashes_sim"),
    };

    let broker_port = env::var("BROKER_PORT")
        .as_ref()
        .map(String::as_str)
        .map_or(Ok(1337), str::parse)
        .expect("Failed to read BROKER_PORT env var");
    match fuzz_fn(
        &[PathBuf::from("./corpus_seed")],
        &[PathBuf::from("./corpus_cache")],
        crash_dir,
        broker_port,
    ) {
        Ok(_) => {},
        Err(Error::ShuttingDown) => {}
        Err(_) => panic!("An error occurred while fuzzing")
    }
}

// Fuzzer for secondary target (hardware, blackbox)
fn hw_fuzz(_seed_dirs: &[PathBuf], _corpus_dirs: &[PathBuf],
           objective_dir: PathBuf, broker_port: u16) -> Result<(), Error> {
    static mut SIGNALS: [u8; 16] = [0; 16];
    // The wrapped harness function, calling out to the LLVM-style harness
    let mut harness = |input: &BytesInput| {
        let target = input.target_bytes();
        let buf = target.as_slice();
        libfuzzer_test_one_input_hw(buf);
        unsafe { SIGNALS[0] = 1 };
        ExitKind::Ok
    };
    let monitor = MultiMonitor::new(|s| println!("{}", s));
    let time_observer = TimeObserver::new("time");
    let observer = StdMapObserver::new("signals", unsafe { &mut SIGNALS });
    let feedback_state = MapFeedbackState::with_observer(&observer);
    let feedback = feedback_or!(
        MaxMapFeedback::new(&feedback_state, &observer),
        TimeFeedback::new_with_observer(&time_observer)
    );
    let objective = CrashFeedback::new();
    let (state, mut mgr) =
        match setup_restarting_mgr_std(monitor, broker_port, EventConfig::AlwaysUnique) {
            Ok(res) => res,
            Err(Error::ShuttingDown) => return Ok(()),
            Err(e) => panic!("Failed to setup the restarter: {}", e),
        };
    let mut state = state.unwrap_or_else(|| {
        StdState::new(
            StdRand::with_seed(current_nanos()),
            InMemoryCorpus::new(),
            OnDiskCorpus::new(objective_dir).unwrap(),
            tuple_list!(feedback_state),
        )});

    //let mutator = StdScheduledMutator::new(havoc_mutations());
    //let scheduler = QueueScheduler::new();
    //let scheduler = libafl::schedulers::EventScheduler::new();
    //let mut mgr = SimpleEventManager::new(monitor);
    let mutator = libafl::mutators::StdNoopMutator::new();
    let mut stages = tuple_list!(libafl::stages::noop::NoopMutationalStage::new(mutator));
    let scheduler = libafl::schedulers::StopScheduler::new();
    let mut fuzzer = StdFuzzer::new(scheduler, feedback, objective);
    fuzzer.register_as_hw_fuzzer();
    let mut executor = InProcessExecutor::new(
        &mut harness,
        tuple_list!(observer, time_observer),
        &mut fuzzer,
        &mut state,
        &mut mgr,
    ).expect("Failed to create the Executor");

    let args: Vec<String> = env::args().collect();
    if libfuzzer_initialize_hw(&args) == -1 {
        println!("Warning: LLVMFuzzerInitialize failed with -1")
    }

    let seed: &[u8] = &[1,2,3];
    fuzzer.add_input(&mut state, &mut executor, &mut mgr, seed.into())?;

    //fuzzer.fuzz_one(&mut stages, &mut executor, &mut state, &mut mgr)?;
    loop {
        match fuzzer.fuzz_loop_for(&mut stages, &mut executor, &mut state, &mut mgr, 10) {
            Ok(_) => {},
            Err(Error::Empty(_s)) => {
                //println!("Corpus exhausted: {}", s);
            },
            Err(_) => {
                break;
            },
        }

        use libafl::events::EventProcessor;
        // this can actually become the main loop, if I add an Event::HwTestCase in llmp.rs.
        // but maybe better to do it some other way so we get the crash observers.
        while let Ok(count) = mgr.process(&mut fuzzer, &mut state, &mut executor) {
            if count == 0 {
                thread::sleep(Duration::from_millis(100));
            }
        }
        break;
    }
    Ok(())
}

fn add_calibration_inputs<I,S,Z,E,EM>(
    state: &mut S,
    fuzzer: &mut Z,
    executor: &mut E,
    manager: &mut EM,
) -> Result<(), Error>
    where
    I: libafl::inputs::Input,
    Z: Evaluator<E, EM, I, S>,
{
    let in_dir = "calibration";
    // sort the files, since calibration order matters
    let mut entries: Vec<_> = std::fs::read_dir(in_dir)?.map(|p| p.unwrap()).collect();
    entries.sort_by_key(|p| p.path());
    for entry in entries {
        let path = entry.path();
        let attributes = std::fs::metadata(&path);
        if attributes.is_err() {
            continue;
        }
        let attr = attributes?;
        if attr.is_file() && attr.len() > 0 {
            let input = I::from_file(&path)?;
            println!("calibratation file: {:?}", path);
            let _ = fuzzer.add_input(state, executor, manager, input)?;
        }
    }
    Ok(())
}

/// Fuzzer for primary target (simulator, greybox)
fn fuzz(seed_dirs: &[PathBuf], corpus_dirs: &[PathBuf],
        objective_dir: PathBuf, broker_port: u16) -> Result<(), Error> {
    let monitor = MultiMonitor::new(|s| println!("{}", s));
    let (state, mut restarting_mgr) =
        match setup_restarting_mgr_std(monitor, broker_port, EventConfig::AlwaysUnique) {
            Ok(res) => res,
            Err(err) => match err {
                Error::ShuttingDown => return Ok(()),
                _ => panic!("Failed to setup the restarter: {}", err),
            },
        };
    println!("Running fuzzer against primary target (simulator)");

    // default to a minimum 0.25% of testcases sent to hardware, and
    // allow overriding with environment variable
    let hw_ratio: f64 = env::var("HWRATIO")
        .as_ref()
        .map(String::as_str)
        .map_or(Ok(0.25), str::parse)
        .expect("Failed to read HWRATIO env var");

    let edges = unsafe { &mut EDGES_MAP[0..MAX_EDGES_NUM] };
    let edges_observer = HitcountsMapObserver::new(StdMapObserver::new("edges", edges));
    let stores_observer = unsafe { StoresObserver::new("stores", 2, 5,
                                                       hw_ratio,
                                                       &CALIBRATING_STORES,
                                                       STORES_CACHE.clone()) };
    let time_observer = TimeObserver::new("time");
    let feedback_state = MapFeedbackState::with_observer(&edges_observer);
    let feedback = feedback_or!(
        MaxMapFeedback::new_tracking(&feedback_state, &edges_observer, true, false),
        //HwFeedback::new(RandFeedback::new("hwfeed", 3)),
        TimeFeedback::new_with_observer(&time_observer),
        HwFeedback::new(StoresFeedback::new_with_observer(&stores_observer))
    );
    let objective = feedback_or_fast!(CrashFeedback::new(), TimeoutFeedback::new());
    let mut state = state.unwrap_or_else(|| {
        StdState::new(
            StdRand::with_seed(current_nanos()),
            CachedOnDiskCorpus::new((&corpus_dirs[0]).clone(), 100).unwrap(),
            OnDiskCorpus::new(objective_dir).unwrap(),
            tuple_list!(feedback_state),
        )
    });

    let mutator = StdMOptMutator::new(&mut state, havoc_mutations(), 5)?;
    //let mutator = libafl::mutators::StdScheduledMutator::new(havoc_mutations());

    //let calibration = CalibrationStage::new(&edges_observer);
    //let power =
    //    StdPowerMutationalStage::new(&mut state, mutator,
    //                                 &edges_observer, PowerSchedule::FAST);
    //
    ////let calstores = unsafe { CalibrateStoresStage::new(2, &CALIBRATING_STORES) };
    //let mut stages = tuple_list!(
    //    // TODO: a fucking boatload of mutations runs between each stage!
    //    // could move this into a HwFeedback
    //
    //    //calstores,
    //    calibration,
    //    //calibration2,
    //    power,
    //    //power2
    //);
    //
    //// A minimization+queue policy to get testcasess from the corpus
    //let scheduler = IndexesLenTimeMinimizerScheduler::new(StdWeightedScheduler::new());

    let mut stages = tuple_list!(StdMutationalStage::new(mutator));
    let scheduler = libafl::schedulers::RandScheduler::new();

    // A fuzzer with feedbacks and a corpus scheduler
    let mut fuzzer = StdFuzzer::new(scheduler, feedback, objective);

    // The wrapped harness function, calling out to the LLVM-style harness
    let mut harness = |input: &BytesInput| {
        let target = input.target_bytes();
        let buf = target.as_slice();
        libfuzzer_test_one_input(buf);
        ExitKind::Ok
    };

    let mut executor = TimeoutExecutor::new(
        InProcessExecutor::new(
            &mut harness,
            tuple_list!(edges_observer,
                        stores_observer,
                        time_observer),
            &mut fuzzer,
            &mut state,
            &mut restarting_mgr,
        )?,
        Duration::new(600, 0),
    );

    let args: Vec<String> = env::args().collect();
    if libfuzzer_initialize(&args) == -1 {
        println!("Warning: LLVMFuzzerInitialize failed with -1")
    }

    if state.corpus().count() < 1 {
        // add calibration inputs first, which calibrate the
        // StoresFeedback.  order matters, these must come first.
        add_calibration_inputs(&mut state, &mut fuzzer,
                               &mut executor, &mut restarting_mgr)
            .expect("Couldn't load calibration inputs");

        // force-import mandatory seeds
        state
            .load_initial_inputs_forced(&mut fuzzer, &mut executor,
                                        &mut restarting_mgr, seed_dirs)
            .unwrap_or_else(|_| panic!("Failed to load initial corpus at {:?}",
                                       &corpus_dirs));

        // import the saved corpus from disk.
        state
            .load_initial_inputs(&mut fuzzer, &mut executor,
                                 &mut restarting_mgr, corpus_dirs)
            .unwrap_or_else(|_| panic!("Failed to load initial corpus at {:?}",
                                       &corpus_dirs));
    }

    let iters = 20_000_000;
    fuzzer.fuzz_loop_for(
        &mut stages,
        &mut executor,
        &mut state,
        &mut restarting_mgr,
        iters,
    )?;
    Ok(())
}

/// Fuzzer for single target (blackbox)
fn fuzz_single_blackbox(seed_dirs: &[PathBuf], corpus_dirs: &[PathBuf],
                        objective_dir: PathBuf, broker_port: u16) -> Result<(), Error> {
    let monitor = MultiMonitor::new(|s| println!("{}", s));
    let (state, mut restarting_mgr) =
        match setup_restarting_mgr_std(monitor, broker_port, EventConfig::AlwaysUnique) {
            Ok(res) => res,
            Err(err) => match err {
                Error::ShuttingDown => { return Ok(()); },
                _ => { panic!("Failed to start manager: {}", err); }
            },
        };

    println!("Starting a single-target blackbox fuzzer!");

    let time_observer = TimeObserver::new("time");
    //let edges = unsafe { &mut EDGES_MAP[0..MAX_EDGES_NUM] };
    //let edges_observer = HitcountsMapObserver::new(StdMapObserver::new("edges", edges));
    //let feedback_state = MapFeedbackState::with_observer(&edges_observer);
    let feedback = feedback_or!(
        //MaxMapFeedback::new_tracking(&feedback_state, &edges_observer, true, false),
        //RandFeedback::new("rand", 5),
        //RandomMapFeedback::new_tracking(&feedback_state, &edges_observer, true, false),
        TimeFeedback::new_with_observer(&time_observer)
    );
    let objective = feedback_or_fast!(CrashFeedback::new(), TimeoutFeedback::new());

    let mut state = state.unwrap_or_else(|| {
        StdState::new(
            StdRand::with_seed(current_nanos()),
            CachedOnDiskCorpus::new((&corpus_dirs[0]).clone(), 100).unwrap(),
            OnDiskCorpus::new(objective_dir).unwrap(),
            tuple_list!(
                //feedback_state
            ),
        )
    });

    let mutator = StdMOptMutator::new(&mut state, havoc_mutations(), 5)?;
    //let calibration = CalibrationStage::new(&edges_observer);
    //let power = StdPowerMutationalStage::new(&mut state, mutator,
    //                                         &edges_observer, PowerSchedule::FAST);
    //let mut stages = tuple_list!(calibration, power);
    //let scheduler = IndexesLenTimeMinimizerScheduler::new(StdWeightedScheduler::new());


    // NOTE: this mutator is nearly twice as fast as the MOpt mutator...
    //let mutator = libafl::mutators::StdScheduledMutator::new(havoc_mutations());
    let mut stages = tuple_list!(StdMutationalStage::new(mutator));
    let scheduler = libafl::schedulers::RandScheduler::new();

    let mut fuzzer = StdFuzzer::new(scheduler, feedback, objective);
    fuzzer.register_as_blackbox_fuzzer();
    let mut harness = |input: &BytesInput| {
        let target = input.target_bytes();
        let buf = target.as_slice();
        libfuzzer_test_one_input_hw(buf);
        ExitKind::Ok
    };

    let mut executor = TimeoutExecutor::new(
        InProcessExecutor::new(
            &mut harness,
            tuple_list!(
                //edges_observer,
                time_observer),
            &mut fuzzer,
            &mut state,
            &mut restarting_mgr,
        )?,
        Duration::new(600, 0),
    );

    let args: Vec<String> = env::args().collect();
    if libfuzzer_initialize_hw(&args) == -1 {
        println!("Warning: LLVMFuzzerInitialize failed with -1")
    }

    if state.corpus().count() < 1 {
        // add calibration inputs first, which calibrate the
        // StoresFeedback.  order matters, these must come first.
        add_calibration_inputs(&mut state, &mut fuzzer,
                               &mut executor, &mut restarting_mgr)
            .expect("Couldn't load calibration inputs");

        // force-import mandatory seeds
        state
            .load_initial_inputs_forced(&mut fuzzer, &mut executor,
                                        &mut restarting_mgr, seed_dirs)
            .unwrap_or_else(|_| panic!("Failed to load initial seeds at {:?}",
                                       &corpus_dirs));

        // import the saved corpus from disk.
        state
            .load_initial_inputs(&mut fuzzer, &mut executor,
                                 &mut restarting_mgr, corpus_dirs)
            .unwrap_or_else(|_| panic!("Failed to load initial corpus at {:?}",
                                       &corpus_dirs));
    }

    let iters = 6_000_000;
    //let iters = 400_000;
    fuzzer.fuzz_loop_for(
        &mut stages,
        &mut executor,
        &mut state,
        &mut restarting_mgr,
        iters,
    )?;
    Ok(())
}
