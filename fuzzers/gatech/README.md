# Zebrafuzz

This folder contains the `zebrafuzz` multi-target fuzzer.  It is a hybrid fuzzer prototype designed to fuzz USB Security Tokens.  Two targets and harnesses are required: a simulator with LLVM's `pc-guards` and `trace-stores` instrumentation enabled, and an uninstrumented hardware target, with the harness handling USB communication.

See the included Makefile for examples of building.  See my `zebrasim` repository for an evaluation target capable of testing `zebrafuzz`.
