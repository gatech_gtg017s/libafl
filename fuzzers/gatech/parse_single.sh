#!/bin/bash
set -e

if [[ $# -ne 1 ]]; then
    echo "usage: $0 <idx>"
    exit 1
fi

IDX=$1

EXEC_ARR=(`cat single_${IDX}_broker.log |grep "CLIENT" |tail -n1 |sed 's/.*executions: \([0-9]*\), exec[/]sec: \([0-9]*\).*/\1 \2/'`)
EXEC_TOTAL=${EXEC_ARR[0]}
EXEC_RATE=${EXEC_ARR[1]}

SECONDS=`cat single_${IDX}_broker.log |grep GLOBAL | tail -n1 |sed 's/.*run time: \([0-9]*\)h[-]\([0-9]*\)m[-]\([0-9]*\)s.*/\1 \2 \3/' | awk '{print $1*60*60 + $2*60 + $3}'`

ABORT_COUNT=`grep -A10 ABORTs single_${IDX}_primary.log | wc -l`
ABORTS="|||||||||||||||||||"
if [[ $ABORT_COUNT -eq 11 ]]; then
    ABORTS=`grep -A10 ABORTs single_${IDX}_primary.log |grep -v ABORT | sed 's/  - \([0-9]*\): \([0-9]*\) [(]\([0-9]*\).*/\3 \2/' | paste -sd" " |tr " " "|"`
fi

echo $ABORTS | awk -v id=$IDX -v exec_total=$EXEC_TOTAL -v exec_rate=$EXEC_RATE -v seconds=$SECONDS 'BEGIN{ORS=""}{printf "|%s|0|",id ; print ; printf "|%s|%s|%s||||\n",seconds,exec_total,exec_rate }' |awk -F'|' 'BEGIN{OFS="|"} {for (i=1;i<NF;i++) { if ($i ~ /^[0-9]+$/ && $i > 18446744072000000000) {$i=""} } ; print}'
