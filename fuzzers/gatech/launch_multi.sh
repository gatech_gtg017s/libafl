#!/bin/bash
set -e

if [[ $# -ne 2 ]]; then
    echo "usage: $0 <id#> <ratio>"
    exit 1
fi

ID=$1
RATIO=$2
RETVAL=1

rm -f corpus_cache/*
rm -f crashes_hw/*
rm -f crashes_sim/*

function exit_handler {
    sleep 0.5
    RUNNING=`pidof zebrafuzz | wc -w`
    if [[ $RUNNING -lt 5 ]]; then
        if [[ $RUNNING -ne 0 ]]; then
            pkill -f zebrafuzz
        fi
    fi
    echo "Done!"
    exit $RETVAL
}
trap exit_handler EXIT

./zebrafuzz > "multi_r${RATIO}_${ID}_broker.log" 2>&1 &
PID_BROKER=$!
sleep 0.5

HWFUZZ=1 taskset -c 4 ./zebrafuzz --applet-idx=224 > "multi_r${RATIO}_${ID}_secondary.log" 2>&1 &
PID_SECONDARY=$!
sleep 0.5

HWRATIO=$RATIO taskset -c 3 ./zebrafuzz --applet-idx=224 > "multi_r${RATIO}_${ID}_primary.log" 2>&1 &
PID_PRIMARY=$!
sleep 0.5

echo "Running pids: $PID_BROKER / $PID_PRIMARY / $PID_SECONDARY"

while [[ 1 ]]; do
    RUNNING=`pidof zebrafuzz | wc -w`
    if [[ $RUNNING -lt 5 ]]; then
        echo "At least one process exited, shutting down ($RUNNING procs)"
        set +e
        kill $PID_BROKER 2>&1 >/dev/null
        kill $PID_SECONDARY 2>&1 >/dev/null
        kill $PID_PRIMARY 2>&1 >/dev/null
        set -e
        break
    fi
    sleep 0.3
done

notify-send -u critical "zebrafuzz" "Fuzz evaluation multi_${ID} finished"
RETVAL=0
